# Omnibus GitLab Management Scripts

This repository contains some scripts and information for easy management of a GitLab installation using the Omnibus docker image.

This setup has been tested on
  * Standard Debian and Ubuntu based Virtual Machines
  * Synology NAS with DSM with Docker Support (See https://mlohr.com/gitlab-on-a-diskstation/ for instructions on how to run GitLab on Synology DSM)


## Requirements

  * docker
  * docker-compose
  * curl
  * jq


## Setup Instructions

  * Copy `docker-compose.yml`, `update-gitlab.sh` and `docker-image-update-check.sh` to the directory where you [want to] have your GitLab data.
  * Use `./update-gitlab.sh` for a manual GitLab update.
  * Create a cronjob calling `cd /path/to/gitlab/; ./docker-image-update-check.sh gitlab/gitlab-ce ./update-gitlab.sh` for automatic GitLab update.
