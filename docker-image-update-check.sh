#!/bin/bash


# Tools, helper scripts and some information to manage self-hosted Omnibus GitLab instances.
# Copyright (C) 2021 Matthias Lohr <mail@mlohr.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Example usage:
# ./docker-image-update-check.sh gitlab/gitlab-ce update-gitlab.sh

IMAGE_ABSOLUTE="$1" ; shift
UPDATE_COMMAND="$@"

# check if all required tools are installed
TOOLS_REQUIRED="curl docker jq"
for TOOL_NAME in $TOOLS_REQUIRED ; do
    TOOL_PATH=$(which $TOOL_NAME)
    if [ -z "$TOOL_PATH" ] ; then
        echo "Could not find required tool: $TOOL_NAME" 1>&2
        echo "To use this script, you need to install the following tools:" 1>&2
        echo "  $TOOLS_REQUIRED" 1>&2
        exit 1
    fi
done

# from now on fail directly when subcommand fails
set -e

# check if first part of image name contains a dot, then it's a registry domain and not from hub.docker.com
if [[ $(echo $IMAGE_ABSOLUTE | cut -d : -f 1 | cut -d / -f 1) == *"."* ]] ; then
    IMAGE_REGISTRY=$(echo $IMAGE_ABSOLUTE | cut -d / -f 1)
    IMAGE_REGISTRY_API=$IMAGE_REGISTRY
    IMAGE_PATH_FULL=$(echo $IMAGE_ABSOLUTE | cut -d / -f 2-)
elif [[ $(echo $IMAGE_ABSOLUTE | awk -F"/" '{print NF-1}') == 0 ]] ; then
    IMAGE_REGISTRY="docker.io"
    IMAGE_REGISTRY_API="registry-1.docker.io"
    IMAGE_PATH_FULL=library/$IMAGE_ABSOLUTE
else
    IMAGE_REGISTRY="docker.io"
    IMAGE_REGISTRY_API="registry-1.docker.io"
    IMAGE_PATH_FULL=$IMAGE_ABSOLUTE
fi

# detect image tag
if [[ "$IMAGE_PATH_FULL" == *":"* ]] ; then
    IMAGE_PATH=$(echo $IMAGE_PATH_FULL | cut -d : -f 1)
    IMAGE_TAG=$(echo $IMAGE_PATH_FULL | cut -d : -f 2)
    IMAGE_LOCAL="$IMAGE_ABSOLUTE"
else
    IMAGE_PATH=$IMAGE_PATH_FULL
    IMAGE_TAG="latest"
    IMAGE_LOCAL="$IMAGE_ABSOLUTE:latest"
fi

# printing full image information
echo "Checking for available update for $IMAGE_REGISTRY/$IMAGE_PATH:$IMAGE_TAG..."

# check local digest first
DIGEST_LOCAL=$(docker images -q --no-trunc $IMAGE_LOCAL)
if [ -z "${DIGEST_LOCAL}" ] ; then
    echo "Local digest: not found" 1>&2
    echo "For security reasons, this script only allows updates of already pulled images." 1>&2
    exit 1
fi
echo "Local digest:  ${DIGEST_LOCAL}"

# check remote digest
AUTH_DOMAIN_SERVICE=$(curl --head "https://${IMAGE_REGISTRY_API}/v2/" 2>&1 | grep realm | cut -f2- -d "=" | tr "," "?" | tr -d '"' | tr -d "\r")
AUTH_SCOPE="repository:${IMAGE_PATH}:pull"
AUTH_TOKEN=$(curl --silent "${AUTH_DOMAIN_SERVICE}&scope=${AUTH_SCOPE}&offline_token=1&client_id=shell" | jq -r '.token')
DIGEST_RESPONSE=$(curl --silent -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
    -H "Authorization: Bearer ${AUTH_TOKEN}" \
    "https://${IMAGE_REGISTRY_API}/v2/${IMAGE_PATH}/manifests/${IMAGE_TAG}")
RESPONSE_ERRORS=$(jq -r "try .errors[] // empty" <<< $DIGEST_RESPONSE)
if [[ -n $RESPONSE_ERRORS ]]; then
    echo "Error during API request occurred: $(echo "$RESPONSE_ERRORS" | jq -r .message)" 1>&2
    exit 1
fi
DIGEST_REMOTE=$(jq -r ".config.digest" <<< $DIGEST_RESPONSE)

echo "Remote digest: ${DIGEST_REMOTE}"


# compare digests
if [ "$DIGEST_LOCAL" != "$DIGEST_REMOTE" ] ; then
    echo "Update available. Executing update command."
    $UPDATE_COMMAND
else
    echo "Already up to date. Nothing to do."
fi
