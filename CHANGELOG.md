# Changelog

## v1.1.0

  * Added shortcut script for registry garbage collection (`gitlab-registry-garbage-collect.sh`)
  * Removed `sudo` from `update-gitlab.sh`, should be executed with sudo by the user if required

## v1.0.0

  * First extensively tested version of the `docker-image-update-check.sh` script.
    Many thanks to @DevDavido!
