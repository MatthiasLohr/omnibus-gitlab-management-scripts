#!/bin/sh


# Tools, helper scripts and some information to manage self-hosted Omnibus GitLab instances.
# Copyright (C) 2021 Matthias Lohr <mail@mlohr.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


echo "Download new image..."
docker pull gitlab/gitlab-ce:latest
echo "Cleaning up old registry images..."
docker exec gitlab gitlab-ctl registry-garbage-collect > /dev/null
echo "Creating backup..."
docker exec gitlab gitlab-rake gitlab:backup:create SKIP=artifacts,registry
echo "Apply updates..."
docker-compose up -d
